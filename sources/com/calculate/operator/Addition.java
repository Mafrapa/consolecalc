package com.calculate.operator;

import java.util.regex.Pattern;

/**
 * This is simple Addition class,
 * with two values.
 * 
 * 
 * @author Maciej
 *
 */
public class Addition {

	// Declaration and Init
	private double value1 = 0.00;
	private double value2 = 0.00;
	private double result = 0;
	private String operator = "+";
	
	// Standard constructor
	public Addition() {
		
	}
	
	// Constructor with arguments
	public Addition(String value1, String value2) {
		
		if(!value1.isEmpty() || !value2.isEmpty())
		{
			try {

				boolean matchValue1 = Pattern.matches("\\d+" , value1);
				boolean matchValue2 = Pattern.matches("\\d+" , value1);
				
				
				if(matchValue1 && matchValue2 ) {
					
					this.value1 = Double.parseDouble(value1);
					this.value2 = Double.parseDouble(value2);					
				}
				
				else 
					System.out.println("Invalid values");
					
					
			}
			catch(NumberFormatException error) {
				
				System.out.println(error.getStackTrace());
				
			}		

		} else
			System.out.println("Invalid operator!");
			return;
	}

	
	public double additionResult() {
		double x = this.value1;
		double y = this.value2;
		
		System.out.print(x + " + " + y + " = ");

		
		return result = x + y;
	}
}
