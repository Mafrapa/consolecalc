package com.calculate.operator;

import java.util.regex.Pattern;

public class Division {


	// Declaration and Init
	private double value1 = 0.00;
	private double value2 = 0.00;
	private double result = 0;
	private String operator = "/";
	
	// Standard constructor
	public Division() {
		
	}
	
	// Constructor with arguments
	public Division(String value1, String value2) {		
		
		if(!value1.isEmpty() || !value2.isEmpty())
		{
			try {

				boolean matchValue1 = Pattern.matches("\\d+" , value1);
				boolean matchValue2 = Pattern.matches("\\d+" , value2);
				
				
				if(matchValue1 && matchValue2 ) {
					
					this.value1 = Double.parseDouble(value1);
					this.value2 = Double.parseDouble(value2);
				
					
				}
				
				else {
					System.out.println("Invalid operator!");
					return;
				}
				// TODO
					
			}
			catch(NumberFormatException error) {
				
				System.out.println(error.getMessage());
				
			}
			
			
			

		} else
			return;

	}

	
	public void divisionResult() {
		double x = this.value1;
		double y = this.value2;

		if(x == 0 || y == 0) {
			System.out.println("Divided by zero error!");
			return;
		}
		
		else {
		 
		result = x / y;
		System.out.print(x + " / " + y + " = ");
		System.out.println(result);
		}
		
	}

	
}
