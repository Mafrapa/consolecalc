package com.calculate.operator;

import java.util.regex.Pattern;

public class Multiplikation {

	// Declaration and Init
	private double value1 = 0.00;
	private double value2 = 0.00;
	private double result = 0;
	private String operator = "*";
	
	// Standard constructor
	public Multiplikation() {
		
	}
	
	// Constructor with arguments
	public Multiplikation(String value1, String value2) {
				
		
		if(!value1.isEmpty() || !value2.isEmpty())
		{
			try {

				boolean matchValue1 = Pattern.matches("\\d+" , value1);
				boolean matchValue2 = Pattern.matches("\\d+" , value1);
				
				
				if(matchValue1 && matchValue2 ) {
					
					this.value1 = Double.parseDouble(value1);
					this.value2 = Double.parseDouble(value2);
					
				}
				
				else 
					System.out.println("Invalid operator!");
					
			}
			catch(NumberFormatException error) {
				
				System.out.println(error.getMessage());
				
			}
			
			
			

		} else
			return;

	}

	
	public double multiplicationResult() {
		double x = this.value1;
		double y = this.value2;
		
		System.out.print(x + " * " + y + " = ");
		
		return result = x * y;
	}

}
