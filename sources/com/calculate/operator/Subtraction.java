package com.calculate.operator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * This is simple Subtraction class,
 * with two values.
 * 
 * @author Maciej
 *
 */
public class Subtraction {

	// Declaration and Init
	private double value1 = 0.00;
	private double value2 = 0.00;
	private double result = 0;
	private String operator = "-";
	
	// Standard constructor
	public Subtraction() {
		
	}
	
	// Constructor with arguments
	public Subtraction(String value1, String value2) {
		
		boolean p = Pattern.matches("[/d]", value1);
		
		
		if(operator.equals(this.operator) && (!value1.isEmpty() || !value2.isEmpty()))
		{
			try {

				boolean matchValue1 = Pattern.matches("\\d+" , value1);
				boolean matchValue2 = Pattern.matches("\\d+" , value1);
				
				
				if(matchValue1 && matchValue2 ) {
					
					this.value1 = Double.parseDouble(value1);
					this.value2 = Double.parseDouble(value2);					
				}
				
				else 
					System.out.println("Invalid values");
					
			}
			catch(NumberFormatException error) {
				
				System.out.println(error.getStackTrace());
				
			}		

		} else
			System.out.println("Invalid operator!");
			return;

	}

	
	public double subtractionResult() {
		double x = this.value1;
		double y = this.value2;
		
		System.out.print(x + " - " + y + " = ");

		return result = x - y;
	}
}
