package com.calculate;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.calculate.operator.Addition;
import com.calculate.operator.Division;
import com.calculate.operator.Multiplikation;
import com.calculate.operator.Subtraction;

public class consoleCalc {

	private static boolean closeApp = false;
	
	private static Addition add = null;
	private static Subtraction sub = null;
	private static Multiplikation mul = null;
	private static Division div = null;
	static CharSequence operators = "+-*/";
	
	public static void main(String[] args) throws IOException {		
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("---Simple Calculator Console App ---");
		
		do {

				System.out.println("Please type first value:");
				String value1 = reader.readLine();									
				System.out.println("Please type second value:");
				String value2 = reader.readLine();
				System.out.println("Please type calculate operator: ( + , - , * , / )");
				String operator = reader.readLine();
				
				
				if(isNumber(value1) && isNumber(value2)) {
					
					if (!((String)operators).contains(operator)) {
						
						System.out.println("Sorry you put invalid operator. Restart App and try again.");
						quit();
					}
					 
						switch (operator) {
						case "+":
							add = new Addition(value1, value2);
							System.out.println(add.additionResult());
							break;
						case "-":
							sub = new Subtraction(value1, value2);
							System.out.println(sub.subtractionResult());
							break;
						case "*":
							mul = new Multiplikation(value1, value2);
							System.out.println(mul.multiplicationResult());
							break;	
						case "/":
							div = new Division(value1, value2);
							div.divisionResult();
							break;	
						}
					}
				
				
				System.out.println("Do You wanna quit? N(o) or Enter to quit");
				String quit = reader.readLine();
				
				if(quit.equals("N") || quit.equals("n")) {
					 closeApp = false;					
				}
				else
					quit();
				
			}
			
			while(!closeApp);		
		
	}		

	private static boolean isNumber(String value) {
		
		try {
			double d = Double.parseDouble(value);
		} catch (NumberFormatException e) {
			System.out.println("Invalid values!");
			return false;
		}
		return true;
	}
	
	private static void quit() {
		System.out.println("Bye bye...!");
		System.exit(0);
	}
}
				
			
				
		
			

	
